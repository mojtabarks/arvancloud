<?php

namespace App\Http\Requests;

class IsWinnerRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->merge([
            'code' => traverse_english($this->input('code')),
            'mobile' => traverse_english($this->input('mobile')),
        ]);

        return [
            'code' => 'required|exists:codes,code',
            'mobile' => 'required|min:10|max:11|regex:/^09/'
        ];
    }
}
