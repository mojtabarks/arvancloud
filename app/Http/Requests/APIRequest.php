<?php

namespace App\Http\Requests;

use App\Exceptions\ApiValidationException;
use Illuminate\Foundation\Http\FormRequest;

abstract class APIRequest extends FormRequest
{
    /**
     * @param \Illuminate\Contracts\Validation\Validator $validator
     * @throws ApiValidationException
     */
    public function failedValidation($validator)
    {
        throw new ApiValidationException($validator->errors()->all());
    }
}
