<?php

namespace App\Http\Requests;

use App\Rules\NotExists;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Redis;

class SubmitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->merge([
            'code' => traverse_english($this->input('code')),
            'mobile' => traverse_english($this->input('mobile')),
        ]);

        return [
            'code' => ['required' , function ($attribute , $value) {
                $code = Redis::get("code_{$this->code}_limit");
                return !is_null($code);
            }],
            'mobile' => [
                'required',
                'min:10',
                'max:11',
                'regex:/^09/',
                new NotExists('winners')
            ]
        ];
    }
}
