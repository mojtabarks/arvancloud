<?php

namespace App\Http\Controllers;

use App\Http\Requests\IsWinnerRequest;
use App\Repositories\Interfaces\WinnerRepositoryInterface;
use Illuminate\Contracts\View\Factory as ViewFactory;

class WinnerController extends Controller
{
    /**
     * @var WinnerRepositoryInterface
     */
    private $winnerRepository;

    /**
     * WinnerController constructor.
     * @param WinnerRepositoryInterface $winnerRepository
     */
    public function __construct(WinnerRepositoryInterface $winnerRepository)
    {
        $this->winnerRepository = $winnerRepository;
    }

    /**
     * @return ViewFactory
     */
    public function index()
    {
        $winners = $this->winnerRepository->paginated();

        return view('winners.index' ,[
            'winners' => $winners
        ]);
    }

    public function isWinner(IsWinnerRequest $request)
    {
        $is_winner = $this->winnerRepository->isWinner($request);

        return response()->json([
            'is_winner' => $is_winner
        ]);
    }
}
