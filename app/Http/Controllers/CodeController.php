<?php

namespace App\Http\Controllers;

use App\Http\Requests\CodeRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\Factory;
use App\Repositories\Eloquent\CodeRepository;
use App\Repositories\Interfaces\CodeRepositoryInterface;

class CodeController extends Controller
{
    /**
     * @var CodeRepositoryInterface|CodeRepository
     */
    private $codeRepository;

    /**
     * CodeController constructor.
     * @param CodeRepositoryInterface $codeRepository
     */
    public function __construct(CodeRepositoryInterface $codeRepository)
    {
        $this->codeRepository = $codeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory
     */
    public function index()
    {
        $codes = $this->codeRepository
            ->paginated();

        return view('codes.index' , [
            'codes' => $codes
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory
     */
    public function create()
    {
        return view('codes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CodeRequest $request
     * @return RedirectResponse
     */
    public function store(CodeRequest $request)
    {
        $this->codeRepository->store($request);

        return redirect()->route('codes.index');
    }
}
