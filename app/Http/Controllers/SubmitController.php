<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubmitRequest;
use App\Services\Submitter;
use \Illuminate\Http\RedirectResponse;

class SubmitController extends Controller
{
    /**
     * @var Submitter
     */
    private $submitter;

    /**
     * SubmitController constructor.
     * @param Submitter $submitter
     */
    public function __construct(Submitter $submitter)
    {
        $this->submitter = $submitter;
    }

    /**
     * @param SubmitRequest $request
     * @return RedirectResponse
     */
    public function __invoke(SubmitRequest $request)
    {
        $this->submitter = $this->submitter->setRequest($request);

        if ($this->submitter->attempt()) {
            $request->session()->flash('alert-success', 'YOU WON !');
            return redirect()->back();
        }

        $request->session()->flash('alert-danger', 'too slow !');
        return redirect()->back();
    }
}
