<?php

namespace App\Repositories\Eloquent;

use App\Models\Code;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Illuminate\Redis\RedisManager;
use Illuminate\Redis\Connections\PhpRedisConnection;
use App\Repositories\Interfaces\CodeRepositoryInterface;

class CodeRepository extends BaseRepository implements CodeRepositoryInterface
{
    /**
     * @var RedisManager|PhpRedisConnection
     */
    private $redis;

    /**
     * CodeRepository constructor.
     * @param Code $code
     * @param RedisManager $redis
     */
    public function __construct(Code $code, RedisManager $redis)
    {
        parent::__construct($code);
        $this->redis = $redis;
    }

    /**
     * @param Request $request
     * @return Code
     */
    public function store(Request $request): Code
    {
        /** @var Code $code */
        $code = $this->model->create(
            $this->prepareData($request)
        );

        $this->redis->set("code_{$code->code}_limit" , $code->limit);

        return $code;
    }

    /**
     * @return LengthAwarePaginator
     */
    public function index()
    {
        return $this->paginated();
    }

    /**
     * @param Request $request
     * @return array
     */
    private function prepareData(Request $request)
    {
        return [
            'name' => $request->get('name'),
            'code' => $request->get('code'),
            'limit' => $request->get('limit'),
        ];
    }
}
