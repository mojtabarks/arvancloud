<?php

namespace App\Repositories\Eloquent;

use App\Models\Winner;
use App\Repositories\Interfaces\WinnerRepositoryInterface;
use Illuminate\Http\Request;

class WinnerRepository extends BaseRepository implements WinnerRepositoryInterface
{
    public function __construct(Winner $winner)
    {
        parent::__construct($winner);
    }

    /**
     * @param $request
     * @return bool
     */
    public function isWinner(Request $request): bool
    {
        return $this->model
            ->where($request->only(['mobile']))
            ->whereHas('code' , function ($query) use ($request){
                $query->where($request->only(['code']));
            })
            ->exists();
    }
}
