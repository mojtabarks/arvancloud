<?php

namespace App\Repositories\Eloquent;

use App\Repositories\Interfaces\EloquentRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use \Illuminate\Contracts\Pagination\LengthAwarePaginator;

class BaseRepository implements EloquentRepositoryInterface
{
    /**
     * @var Model|Builder
     */
    protected $model;

    /**
     * BaseRepository constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function findBy($attribute, $value): ?Model
    {
        return $this->model->where($attribute , $value)->first();
    }

    /**
     * @return LengthAwarePaginator
     */
    public function paginated(): LengthAwarePaginator
    {
        return $this->model
            ->latest()
            ->paginate();
    }
}
