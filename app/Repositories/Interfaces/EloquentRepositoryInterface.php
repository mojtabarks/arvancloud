<?php

namespace App\Repositories\Interfaces;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;

interface EloquentRepositoryInterface
{
    /**
     * @param $attribute
     * @param $value
     * @return Model|null
     */
    public function findBy($attribute, $value): ?Model;

    /**
     * @return LengthAwarePaginator
     */
    public function paginated():LengthAwarePaginator;


}
