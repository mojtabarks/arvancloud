<?php


namespace App\Repositories\Interfaces;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

interface WinnerRepositoryInterface
{
    /**
     * @return LengthAwarePaginator
     */
    public function paginated(): LengthAwarePaginator;

    /**
     * @param $request
     * @return bool
     */
    public function isWinner(Request $request): bool;
}
