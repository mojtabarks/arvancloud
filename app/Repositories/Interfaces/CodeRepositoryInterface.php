<?php

namespace App\Repositories\Interfaces;

use App\Models\Code;
use Illuminate\Http\Request;

interface CodeRepositoryInterface
{
    /**
     * @param Request $request
     * @return Code
     */
    public function store(Request $request): Code;
}
