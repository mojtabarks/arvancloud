<?php

namespace App\Providers;

use App\Repositories\Eloquent\CodeRepository;
use App\Repositories\Eloquent\WinnerRepository;
use App\Repositories\Interfaces\CodeRepositoryInterface;
use App\Repositories\Interfaces\WinnerRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CodeRepositoryInterface::class , CodeRepository::class);
        $this->app->bind(WinnerRepositoryInterface::class , WinnerRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
