<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Redis;

class NotExists implements Rule
{
    private $table;

    /**
     * Create a new rule instance.
     *
     * @param $table
     */
    public function __construct($table)
    {
        $this->table = $table;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return (!$this->inRedis($attribute, $value));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "You Won Already no need to re-sign!";
    }

    /**
     * @param string $attribute
     * @param $value
     * @return int
     */
    private function inRedis(string $attribute, $value)
    {
        return Redis::get("{$attribute}_{$value}");
    }
}
