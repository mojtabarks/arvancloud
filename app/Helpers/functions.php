<?php

if (!function_exists('traverse_english')) {
    /**
     * @param $str
     * @return mixed
     */
    function traverse_english($str)
    {
        $farsi_chars = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
        $latin_chars = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
        return str_replace($farsi_chars, $latin_chars, $str);
    }
}
