<?php

namespace App\Jobs;

use App\Models\Code;
use App\Models\Winner;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SubmitWinner implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $mobile;
    private $code;

    /**
     * Create a new job instance.
     *
     * @param $mobile
     * @param $code
     */
    public function __construct($mobile, $code)
    {
        $this->mobile = $mobile;
        $this->code = $code;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $code = Code::where('code' , $this->code)->first();

        if ($code instanceof Code) {
            Winner::create([
                'mobile' => $this->mobile,
                'code_id' => $code->id
            ]);
        }
    }
}
