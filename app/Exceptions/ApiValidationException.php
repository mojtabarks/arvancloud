<?php


namespace App\Exceptions;


class ApiValidationException extends \Exception
{
    /**
     * @var array
     */
    private $errors;

    /**
     * ApiValidationException constructor.
     * @param array $errors
     */
    public function __construct(array $errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function render()
    {
        return response()->json([
            'success' => false,
            'message' => 'the given data was invalid',
            'data' => $this->errors
        ]);
    }
}
