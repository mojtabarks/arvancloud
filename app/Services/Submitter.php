<?php

namespace App\Services;

use App\Jobs\SubmitWinner;
use Illuminate\Redis\Connections\PhpRedisConnection;
use Illuminate\Redis\RedisManager;

class Submitter
{
    /**
     * @var RedisManager|PhpRedisConnection
     */
    private $redis;

    protected $request;

    /**
     * Submitter constructor.
     * @param RedisManager $redis
     */
    public function __construct(RedisManager $redis)
    {
        $this->redis = $redis;
    }

    /**
     * @param $request
     * @return Submitter
     */
    public function setRequest($request)
    {
        $this->request = $request;
        return $this;
    }

    /**
     * @return bool
     */
    public function attempt()
    {
        $count = $this->getCount();
        $mobile = $this->request->input('mobile');

        if ($count < $this->getCodeLimit()) {

            $this->redis->set('count_of_' . $this->request->get('code'), $count + 1);
            $this->redis->set("mobile_{$mobile}" , $mobile);

            dispatch(new SubmitWinner(
                $this->request->input('mobile'),
                $this->request->input('code'))
            );

            return true;
        }

        return false;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return intval($this->redis->get('count_of_' . $this->request->get('code')));
    }

    /**
     * @return int
     */
    private function getCodeLimit()
    {
        return intval($this->redis->get('code_'. $this->request->get('code') . '_limit'));
    }
}
