## Installation

1 - install docker and docker-compose on your system

2 - clone this repo

3 - `$ cd arvanCloud/laradock`;

4 - `$ docker-compose run -d nginx mysql phpmyadmin redis`

5 - open `http://localhost:8081/` and create a database with name `arvanCloud`

6 - in root folder `$ cp .env-example .env`

7 - in the .env : 

````
DB_CONNECTION=mysql
DB_HOST=mysql
DB_PORT=3306
DB_DATABASE=arvanCloud
DB_USERNAME=root
DB_PASSWORD=root
````

8 - open `http://localhost` and you will see the welcome page
 
9 - `$ docker-compose exec workspace bash`

10 - `$ composer install`

11 - you can use the `arvanCloud.postman_collection.json` file in the `storage/docs` 

12 - `php artisan queue:work ` to save winners in database

