<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWinnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('winners', function (Blueprint $table) {
            $table->id();
            $table->string('mobile')->index();
            $table->bigInteger('code_id')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();
        });

//        Schema::table('winners', function (Blueprint $table) {
//            $table->foreign('code_id')
//                ->references('id')
//                ->on('codes')
//                ->onDelete('cascade');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('winners', function (Blueprint $table) {
            $table->dropForeign('winners_code_id_foreign');
        });

        Schema::dropIfExists('winners');
    }
}
