<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// view route
Route::view('/', 'welcome');
// resource controller with repository design pattern
Route::resource('codes', 'CodeController')->only(['index', 'create', 'store']);
// single action controller
Route::post('submit', 'SubmitController')->name('submit');

Route::get('winners', 'WinnerController@index')->name('winners.index');
