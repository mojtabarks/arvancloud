@extends('layouts.master')

@push('styles')
    <style>

        * {
            font-family: 'BYekan', Sans-Serif;
            font-size: 14px;
            direction: rtl;
        }

        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>

    <!-- Custom styles for this template -->
    <link href="https://getbootstrap.com/docs/5.0/examples/sign-in/signin.css" rel="stylesheet">

@endpush

@section('content')

        <div class="row">
            <div class="col-12 text-center">
                <a href="{{ route('winners.index') }}" class="h2">winners</a>
                &nbsp;&nbsp;&nbsp;
                <a href="{{ route('codes.index') }}" class="h2">codes</a>
            </div>
            <div class="col-12">
                <div class="form-signin text-center" id="app">
                    <form action="{{ route('submit') }}" method="post">
                        @csrf
                        <label for="mobile" class="visually-hidden">شماره موبایل</label>
                        <input type="text" name="mobile" value="{{ old('mobile') }}" id="mobile" class="form-control"
                               placeholder="شماره موبایل" required autofocus>

                        <label for="code" class="visually-hidden">کد</label>
                        <input type="text" id="code" value="{{ old('code') }}"
                               name="code" class="form-control mb-3" placeholder="کد"
                               required>

                        <button class="w-100 btn btn-lg btn-primary" type="submit">ثبت کد</button>
                    </form>
                </div>
            </div>
        </div>

@endsection
