@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-2">
            <h1>Winners</h1>
        </div>
        <div class="col-10">
            {!! $winners->links() !!}
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Mobile</th>
                    <th scope="col">Created At</th>
                </tr>
                </thead>
                <tbody>
                @foreach($winners as $winner)
                    <tr>
                        <th scope="row">{{ $loop->iteration }}</th>
                        <td>{{ $winner->mobile }}</td>
                        <td>{{ $winner->created_at->diffForHumans() }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
