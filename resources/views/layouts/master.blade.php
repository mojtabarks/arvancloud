@extends('layouts.skeleton')

@section('master')

    @include('partials.messages')

    @yield('content')

@endsection
