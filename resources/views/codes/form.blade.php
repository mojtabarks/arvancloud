<div class="form-group">
    <label for="name">Name : </label>
    <input type="text" name="name" class="form-control" id="name" aria-describedby="nameHelp" placeholder="Enter name">
</div>

<div class="form-group">
    <label for="code">Code : </label>
    <input type="text" class="form-control" name="code" id="code" placeholder="enter the code">
</div>

<div class="form-group">
    <label for="limit">Limit : </label>
    <input type="number" class="form-control" id="limit" name="limit" placeholder="enter the limit">
</div>

<button type="submit" class="btn btn-primary mt-5">Submit</button>
