@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-6">
            <form action="{{ route('codes.store') }}" method="post">
                @csrf

                @include('codes.form')
            </form>
        </div>
    </div>

@endsection
