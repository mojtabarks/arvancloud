@extends('layouts.master')


@section('content')

    <div class="row mb-5">
        <div class="col-3">
            <h1>Codes</h1>
        </div>
        <div class="col-3">
            <a href="{{ route('codes.create') }}" class="btn btn-success w-100">Create code</a>
        </div>
        <div class="col-6">
            {!! $codes->links() !!}
        </div>

    </div>
    <hr>
    <div class="row">
        <div class="col-12">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">name</th>
                    <th scope="col">Code</th>
                    <th scope="col">Limit</th>
                </tr>
                </thead>
                <tbody>
                @foreach($codes as $code)
                    <tr>
                        <th scope="row">{{ $loop->iteration }}</th>
                        <td>{{ $code->name }}</td>
                        <td>{{ $code->code }}</td>
                        <td>{{ $code->limit }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
